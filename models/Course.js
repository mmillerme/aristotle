'use strict';

module.exports = function (sequelize, relationships) {
	var Sequelize = require('Sequelize');

	var Course = sequelize.define('Course',
		{
			title: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true,
				validate: {
					notEmpty: true
				}
			},

			introduction: {
				type: Sequelize.TEXT,
				allowNull: false
			}
		},
		require('./utilities/makeIDReadonly')()
	);

	relationships.push(function (models) {
		Course.hasMany(models.Lesson);
	});

	return Course;
};