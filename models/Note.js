'use strict';

module.exports = function (sequelize, relationships) {
	var Sequelize = sequelize.Sequelize;

	var Note = sequelize.define('Note',
		{
			block: {
				type: Sequelize.INTEGER.UNSIGNED,
				allowNull: false,
				validate: {
					notEmpty: true
				},
				unique: 'chapterBlock'
			},

			content: {
				type: Sequelize.TEXT,
				allowNull: false
			},

			ChapterId: {
				type: Sequelize.INTEGER,
				unique: 'chapterBlock',
				allowNull: false
			}
		},
		require('./utilities/makeIDReadonly')()
	);

	relationships.push(function (models) {
		Note.belongsTo(models.Chapter);
	});

	return Note;
};