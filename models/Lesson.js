'use strict';

module.exports = function (sequelize, relationships) {
	var Sequelize = sequelize.Sequelize;

	var Lesson = sequelize.define('Lesson',
		{
			title: {
				type: Sequelize.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			}
		},
		require('./utilities/makeIDReadonly')()
	);

	relationships.push(function (models) {
		Lesson.belongsTo(models.Course);
		Lesson.hasMany(models.Chapter);
		Lesson.hasMany(models.Question);
	});

	return Lesson;
};