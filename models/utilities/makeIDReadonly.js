'use strict';

module.exports = function (options) {
	options = options || {};
	options.setterMethods = options.setterMethods || {};
	options.setterMethods.id = function (value) {
		this._id = value;
	};
	options.getterMethods = options.getterMethods || {};
	options.getterMethods.id = function () {
		var id = this.getDataValue('id');
		return id || this._id;
	};

	return options;
};