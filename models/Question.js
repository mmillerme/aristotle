'use strict';

module.exports = function (sequelize, relationships) {
	var Sequelize = sequelize.Sequelize;

	var Question = sequelize.define('Question',
		{
			content: {
				type: Sequelize.TEXT,
				allowNull: false
			},

			answers: {
				type: Sequelize.TEXT,
				allowNull: false
			}
		},
		require('./utilities/makeIDReadonly')()
	);

	relationships.push(function (models) {
		Question.belongsTo(models.Lesson);
	});

	return Question;
};