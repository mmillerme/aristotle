'use strict';

module.exports = function (sequelize, relationships) {
	var Sequelize = sequelize.Sequelize;

	var Chapter = sequelize.define('Chapter',
		{
			title: {
				type: Sequelize.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			},

			body: {
				type: Sequelize.TEXT,
				allowNull: false
			}
		},
		require('./utilities/makeIDReadonly')()
	);

	relationships.push(function (models) {
		Chapter.belongsTo(models.Lesson);
		Chapter.hasMany(models.Note);
	});

	return Chapter;
};