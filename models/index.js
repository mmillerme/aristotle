'use strict';

module.exports = function (sequelize) {
	var models = {};
	var path = require('path');

	var relationships = [];
	require('fs').readdirSync(__dirname).forEach(function (file) {
		if (path.extname(file) == '.js' && file != 'index.js') {
			var model = path.basename(file, '.js');
			models[model] = require('./' + model)(sequelize, relationships);
		}
	});

	relationships.forEach(function (establishRelationships) {
		establishRelationships(models);
	});

	sequelize.sync(/* { force: true } */);

	return models;
};
