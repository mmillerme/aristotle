(function () {
	'use strict';

	define(
		[ 'app' ],
		function () {
			angular.module('Aristotle').factory('Notifications', function (PrimitiveWrapper, $interval) {
				function Notification (text, link, icon) {
					this._id = null;
					this._text = text;
					this._link = link;
					this._icon = icon;
					this._read = false;
				}

				Notification.getID = function () {
					return this._id;
				};

				Notification.prototype.getText = function () {
					return this._text;
				};

				Notification.prototype.setText = function (text) {
					this._text = text;
				};

				Notification.prototype.getLink = function () {
					return this._link;
				};

				Notification.prototype.setLink = function (link) {
					this._link = link;
				};

				Notification.prototype.getIcon = function () {
					return this._icon;
				};

				Notification.prototype.setIcon = function (icon) {
					this._icon = icon;
				};

				Notification.prototype.isRead = function () {
					return this._read;
				};

				Notification.prototype.markAsRead = function () {
					this._read = true;
				};

				var Notifications = {
					Notification: Notification,

					MAX_NOTIFICATIONS: 99,

					_notifications: [],

					_unreadCount: new PrimitiveWrapper(0),

					getNotifications: function () {
						return this._notifications;
					},

					countUnread: function () {
						return this._unreadCount;
					},

					addNotification: function (notification) {
						this._notifications.unshift(notification);
						if (!notification.isRead()) {
							this._unreadCount.setValue(this._unreadCount + 1);
						}

						if (this._notifications.length > Notifications.MAX_NOTIFICATIONS) {
							var poppedNotification = this._notifications.pop();
							if (!poppedNotification.isRead()) {
								this._unreadCount.setValue(this._unreadCount - 1);
							}
						}

						this._updateLocalStorage();
					},

					markAsRead: function (notification) {
						if (!notification.isRead()) {
							notification.markAsRead();
							this._unreadCount.setValue(this._unreadCount - 1);

							this._updateLocalStorage();
						}
					},

					removeNotification: function (notification) {
						this._notifications = $.grep(this._notifications, function (_notification) {
							if (_notification == notification) {
								if (!notification.isRead()) {
									this._unreadCount.setValue(this._unreadCount - 1);
								}
								return false;
							} else {
								return true;
							}
						});

						this._updateLocalStorage();
					},

					enableLocalStorage: function () {
						this._useLocalStorage = true;
					},

					disableLocalStorage: function () {
						this._useLocalStorage = false;
					},

					_updateLocalStorage: function () {
						if (this._useLocalStorage && window.localStorage) {
							window.localStorage.setItem('notifications', JSON.stringify($.map(this._notifications, function (notification) {
								return {
									text: notification.getText(),
									link: notification.getLink(),
									icon: notification.getIcon() || null,
									read: notification.isRead()
								};
							})));
						}
					}
				};

				if (window.localStorage) {
					window.localStorage.setItem('notifications', window.localStorage.getItem('notifications') || '[]');
					Notifications.disableLocalStorage();
					$.each(JSON.parse(window.localStorage.getItem('notifications')).reverse(), function (i, json) {
						var notification = new Notification(json.text, json.link, json.icon || undefined);
						Notifications.addNotification(notification);
						if (json.read) {
							Notifications.markAsRead(notification);
						}
					});
					Notifications.enableLocalStorage();
				}

				return Notifications;
			});
		}
	);
})();