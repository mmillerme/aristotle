(function () {
	'use strict';

	define(
		[ 'app' ],
		function () {
			angular.module('Aristotle').factory('Question', function (CONSTANTS, $resource) {
				return $resource(CONSTANTS.API_ROOT + 'questions/');
			});
		}
	);
})();