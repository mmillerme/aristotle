(function () {
	'use strict';

	define(
		['app'],
		function () {
			angular.module('Aristotle').factory('Lesson', function (CONSTANTS, $resource) {
				return $resource(CONSTANTS.API_ROOT + 'lessons/:id', {}, {
					get: { method: 'GET', cache: true },
					query: { method: 'GET', isArray: true, cache: true }
				});
			});
		}
	);
})();