(function () {
	'use strict';

	define(
		[ 'app' ],
		function () {
			angular.module('Aristotle').factory('Note', function (CONSTANTS, $resource) {
				return $resource(CONSTANTS.API_ROOT + 'notes/:id', {}, {
					update: { method: 'POST' }
				});
			});
		}
	);
})();