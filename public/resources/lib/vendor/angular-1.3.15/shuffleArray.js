(function () {
	'use strict';

	/**
	 * Thanks to Laurens Holst at Stack Overflow
	 * http://stackoverflow.com/a/12646864/546657
	 */

	angular.module('shuffleArray', [])
		.value('shuffleArray', function (array) {
			for (var i = array.length - 1; i > 0; --i) {
				var j = Math.floor(Math.random() * (i + 1));
				var temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
		}
	);
})();