(function () {
	'use strict';

	angular.module('collapsible', []).directive('collapsible', function () {
		return {
			restrict: 'A',
			controller: function ($scope, $timeout) {
				var ctrl = this;

				var collapsed = true;
				var $window = $(window);

				ctrl.expand = function () {
					collapsed = false;

					$timeout(function () {
						$window.on('click', collapse);
					}
					);
				};

				ctrl.collapse = function () {
					collapsed = true;
				};

				ctrl.toggle = function () {
					collapsed ? ctrl.expand() : ctrl.collapse();
				};

				ctrl.isCollapsed = function () {
					return collapsed;
				};

				var collapse = function () {
					$scope.$apply(ctrl.collapse);
					$window.off('click', collapse);
				};

				$scope.$on('$destroy', function () {
					$window.off('click', collapse);
				}
				);
			},
			controllerAs: 'collapsibleCtrl'
		};
	});
})();