(function () {
	'use strict';

	angular.module('affix', []).directive('affix', function () {
		return {
			scope: {
				affixWatch: '=?',
				affixValue: '@',
				affixValueNe: '@'
			},

			link: function ($scope, $element, attrs) {
				$element = $($element);

				var $parent = $element.parent();
				var resizeMaxWidth = function () {
					$element.width($parent.width());
				};

				var $window = $(window);

				var attachAffix = function () {
					$element.affix();
					$window.on('resize', resizeMaxWidth);
					$scope.$on('$destroy', detachAffix);
					resizeMaxWidth();
				};

				var detachAffix = function () {
					$element.removeClass('affix affix-top affix-bottom').removeData('bs.affix').css('width', '');
					$window.off('resize', resizeMaxWidth).off('.affix'); // TODO: will remove other affix elements
				};

				if (attrs.affixWatch === undefined) {
					attachAffix();
				} else {
					$scope.$watch('affixWatch', function (affix) {
						var negate = attrs.affixValue === undefined && attrs.affixValueNe !== undefined;
						var value = (!negate ? attrs.affixValue : attrs.affixValueNe);

						if (value !== undefined) {
							(!negate && affix == value) || (negate && affix != value) ? attachAffix() : detachAffix();
						} else {
							affix ? attachAffix() : detachAffix();
						}
					});
				}
			}
		};
	});
})();