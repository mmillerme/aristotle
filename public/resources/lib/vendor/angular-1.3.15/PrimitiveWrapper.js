(function () {
	'use strict';

	angular.module('PrimitiveWrapper', []).factory('PrimitiveWrapper', function () {
		function PrimitiveWrapper (value) {
			this._value = value;
		}

		PrimitiveWrapper.prototype.getValue = function () {
			return this._value;
		};

		PrimitiveWrapper.prototype.setValue = function (value) {
			this._value = value;
		};

		PrimitiveWrapper.prototype.valueOf = PrimitiveWrapper.prototype.toJSON = PrimitiveWrapper.prototype.getValue;

		PrimitiveWrapper.prototype.toString = function () {
			return this._value.toString();
		};

		return PrimitiveWrapper;
	});
})();