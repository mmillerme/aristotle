(function () {
	'use strict';

	define(
		[ 'app', 'services/Lesson', 'services/Note', 'css!components/lesson/assets/lesson.css' ],
		function () {
			angular.module('Aristotle').controller('LessonController', function ($compile, $scope, $location, $sce, $routeParams, $timeout, Lesson, Note, Notifications) {
				$scope.affix = true;

				var $window = $(window);
				$scope.jumpTo = function (id) {
					var $element = $('#' + id);
					if ($element.length) {
						$window.scrollTop($element.offset().top);
					}
				};

				var $lessonNavContainer = $('#lesson-nav-container');
				var $lessonContent = $('#lesson-content');
				var $document = $(document);
				var offsetLessonNavHeight = function () {
					$lessonContent.css('margin-top', $lessonNavContainer.is(':visible') ? $lessonNavContainer.outerHeight() : 0);
				};
				$window.on('resize', offsetLessonNavHeight);
				$document.on('ariLessonNav.lessonsLoaded', offsetLessonNavHeight);
				offsetLessonNavHeight();

				$scope.lesson = Lesson.get({ id: $routeParams.id }, function () {
					$.each($scope.lesson.Chapters, function (i, chapter) {
						chapter.body = $sce.trustAsHtml(chapter.body);
					});
				});

				$scope.$on('$destroy', function () {
					$window.off('resize', offsetLessonNavHeight);
					$document.off('ariLessonNav.lessonsLoaded', offsetLessonNavHeight);
				});

				$scope.takeNote = function (chapterIndex, blockIndex) {
					var $takeNote = $('> div[ng-repeat] > div[ng-bind-html]', $lesson).eq(chapterIndex)
						.children('p, ul, ol, table, pre').eq(blockIndex).prev().addClass('active');
					var $notePanel = $takeNote.prev();
					$notePanel.show();
					$('textarea', $notePanel).focus().one('blur', function () {
						$takeNote.removeClass('active');
						$notePanel.hide();
					});
				};

				$scope.getNote = function (chapterIndex, blockIndex) {
					var chapter = $scope.lesson.Chapters[chapterIndex];
					chapter.Notes[blockIndex] = chapter.Notes[blockIndex] || {
						block: blockIndex,
						content: '',
						ChapterId: chapter.id
					};
					return chapter.Notes[blockIndex];
				};

				$scope.saveNote = function (chapterIndex, blockIndex) {
					var notes = $scope.lesson.Chapters[chapterIndex].Notes;
					var note = notes[blockIndex];
					if (!note.id && note.content) {
						new Note(note).$save(function (response) {
							note.id = response.id;

							Notifications.addNotification(new Notifications.Notification(
								'You took a note on chapter ' + (chapterIndex + 1) + ' of ' + $scope.lesson.title,
								'/#/retain',
								'glyphicon glyphicon-pencil'
							));
						});
					} else {
						notes[blockIndex] = Note.update({ id: note.id }, note);
					}
				};

				var $lesson = $('.lesson', $lessonContent);
				$scope.$watch('lesson.Chapters', function () {
					$timeout(function () {
						$('.take-note, .note-panel', $lesson).remove();
						$('> div[ng-repeat] > div[ng-bind-html]', $lesson).each(function (i) {
							$(this).children('p, ul, ol, table, pre')
								.each(function (j) {
									var $element = $(this);
									var $notePanel = $('<div class="note-panel"><textarea class="form-control" ng-model="getNote(' + i + ', ' + j + ').content" ng-change="saveNote(' + i + ', ' + j + ')" ng-model-options="{ updateOn: \'blur\' }"></textarea></div>');
									$notePanel = $compile($notePanel)($scope);
									$notePanel.insertBefore($element);

									var $takeNote = $('<a href ng-click="takeNote(' + i + ', ' + j + ')" class="take-note" ng-class="{ \'has-note\': getNote(' + i + ', ' + j + ').content }"><span class="glyphicon glyphicon-pencil"></span></a>');
									$takeNote = $compile($takeNote)($scope);
									$takeNote.insertBefore($element);
								});
						});
					});
				});
			});
		}
	);
})();