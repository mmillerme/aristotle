(function () {
	'use strict';

	define(
		[ 'app', 'services/Note' ],
		function () {
			angular.module('Aristotle').controller('RetainController', function ($scope, Note) {
				$scope.notes = Note.query({ sort: '-updatedAt' });

				var pad = function (number, digits, padder) {
					digits = digits || 2;
					padder = padder || '0';
					number = String(number);
					var padders = digits - number.length;
					return padders > 0 ? new Array(padders + 1).join(padder) + number : number;
				};

				$scope.formatTimestamp = function (timestamp) {
					var date = new Date(timestamp);
					var hours = date.getHours();
					var ampm = 'am';
					if (hours > 12) {
						hours -= 12;
						ampm = 'pm';
					} else if (hours == 0) {
						hours = 12;
					}

					return pad(date.getMonth() + 1) + '/' + pad(date.getDay()) + '/' + pad(date.getFullYear()) +
						' ' + pad(hours) + ':' + pad(date.getMinutes()) + ' ' + ampm;
				};
			});
		}
	);
})();