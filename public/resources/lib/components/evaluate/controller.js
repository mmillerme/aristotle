(function () {
	'use strict';

	define(
		[ 'app', 'css!components/evaluate/assets/quiz.css', 'services/Question', 'services/Notifications' ],
		function () {
			angular.module('Aristotle').controller('EvaluateController', function ($scope, Question, shuffleArray, Notifications) {
				$scope.mode = 'setup';
				$scope.lessonSelections = {};
				$scope.questions = [];
				$scope.answers = {};

				$scope.score = null;
				$scope.calculateScore = function () {
					var correct = 0;
					$.each($scope.questions, function (i, question) {
						if ($scope.answers[question.id] === question.correctAnswer) {
							++correct;
						}
					});

					$scope.score = Math.floor(correct / $scope.questions.length * 100);
				};

				var $window = $(window);
				$scope.$watch('mode', function (mode) {
					$window.scrollTop(0);

					if (mode == 'assess') {
						$scope.questions = [];
						Question.query({ 'lessons[]': $.map($scope.lessonSelections, function (lesson) {
							return lesson.$selected ? lesson.id : undefined;
						}) }, function (questions) {
							shuffleArray(questions);

							if (questions.length > 25) {
								$scope.questions = questions.slice(0, 25);
							}

							$.each($scope.questions, function (i, question) {
								shuffleArray(question.answers);
								$.each(question.answers, function (i, answer) {
									if (answer.correct) {
										question.correctAnswer = i;
										return true;
									}
								});

								$scope.answers[question.id] = null;
							});
						});
					} else if (mode == 'review') {
						$scope.calculateScore();
						Notifications.addNotification(new Notifications.Notification('You scored ' + $scope.score + '% on an assessment', '/#/evaluate', 'glyphicon glyphicon-question-sign'));
					} else if (mode == 'setup') {
						$scope.questions = [];
						$scope.answers = {};
						$scope.score = null;
					}
				});
			});
		}
	);
})();