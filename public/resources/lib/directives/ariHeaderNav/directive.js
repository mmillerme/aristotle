(function () {
	'use strict';

	define(
		[ 'app' ],
		function () {
			angular.module('Aristotle').directive('ariHeaderNav', function (CONSTANTS) {
				return {
					replace: true,
					restrict: 'E',
					scope: {},
					templateUrl: CONSTANTS.DIRECTIVE_VIEW.replace(/%/g, 'ariHeaderNav'),
					controller: function ($scope, $location) {
						$scope.isActive = function (root) {
							return new RegExp('/' + root + '($|/)').test($location.path());
						};
					}
				};
			});
		}
	);
})();