(function () {
	'use strict';

	define(
		[ 'app', 'services/Notifications', 'css!directives/ariNotificationCenter/assets/notification-center.css' ],
		function () {
			angular.module('Aristotle').directive('ariNotificationCenter', function (CONSTANTS, Notifications) {
				return {
					replace: true,
					restrict: 'E',
					scope: {},
					templateUrl: CONSTANTS.DIRECTIVE_VIEW.replace(/%/g, 'ariNotificationCenter'),
					controller: function ($scope) {
						$scope.notifications = Notifications.getNotifications();
						$scope.unreadCount = Notifications.countUnread();
						$scope.markAsRead = function (notification) {
							Notifications.markAsRead(notification);
						};
					}
				};
			});
		}
	);
})();