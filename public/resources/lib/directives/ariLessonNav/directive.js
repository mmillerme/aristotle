(function () {
	'use strict';

	define(
		[ 'app' ],
		function () {
			angular.module('Aristotle').directive('ariLessonNav', function (CONSTANTS, Lesson, $timeout) {
				return {
					replace: true,
					restrict: 'E',
					scope: {
						checkboxes: '@',
						readonly: '=',
						checkAll: '@',
						lessonSelections: '=?'
					},
					templateUrl: CONSTANTS.DIRECTIVE_VIEW.replace(/%/g, 'ariLessonNav'),
					controller: function ($scope, $location) {
						$scope.isActive = function (root) {
							return new RegExp('/' + root + '($|/)').test($location.path());
						};

						$scope.lessonSelections = [];
						$scope.lessons = Lesson.query(function () {
							$timeout(function () {
								$(document).trigger('ariLessonNav.lessonsLoaded');
							});

							$scope.lessonSelections = {};
							$.each($scope.lessons, function (i, lesson) {
								$scope.lessonSelections[lesson.id] = $.extend({}, lesson, { $selected: !!$scope.checkAll });
							});
						});
					}
				};
			});
		}
	);
})();