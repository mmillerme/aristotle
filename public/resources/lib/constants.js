(function () {
	'use strict';

	define(
		[ 'app' ],
		function () {
			angular.module('Aristotle').constant('CONSTANTS', {
				DIRECTIVE_VIEW: '/resources/lib/directives/%/view.html',
				COMPONENT_VIEW: '/resources/lib/components/%/view.html',
				API_ROOT: '/api/',
				COURSE_ID: 1
			});
		}
	);
})();