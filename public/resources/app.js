(function () {
	'use strict';

	define(
		[
			'angular',
			'ngRoute',
			'ngResource',
			'ngSanitize',
			'ui.bootstrap',
			'PrimitiveWrapper',
			'collapsible',
			'scrollSpy',
			'affix',
			'shuffleArray'
		],
		function (angular) {
			angular.module('Aristotle', [
				'ngRoute',
				'ngResource',
				'ngSanitize',
				'ui.bootstrap',
				'PrimitiveWrapper',
				'collapsible',
				'scrollSpy',
				'affix',
				'shuffleArray'
			]);
		}
	);
})();