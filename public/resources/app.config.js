(function () {
	'use strict';

	define(
		[
			'CONSTANTS',

			// Components
			'components/start/controller',
			'components/absorb/controller',
			'components/lesson/controller',
			'components/retain/controller',
			'components/evaluate/controller',

			// Global directives
			'directives/ariHeaderNav/directive',
			'directives/ariNotificationCenter/directive'
		],
		function () {
			angular.module('Aristotle')
				.config(function (CONSTANTS, $routeProvider) {
					$routeProvider
						.when('/start', {
							templateUrl: CONSTANTS.API_ROOT + 'courses/' + CONSTANTS.COURSE_ID + '.introduction',
							controller: 'StartController'
						})
						.when('/absorb', {
							templateUrl: CONSTANTS.COMPONENT_VIEW.replace(/%/g, 'absorb'),
							controller: 'AbsorbController'
						})
						.when('/lessons/:id', {
							templateUrl: CONSTANTS.COMPONENT_VIEW.replace(/%/g, 'lesson'),
							controller: 'LessonController'
						})
						.when('/retain', {
							templateUrl: CONSTANTS.COMPONENT_VIEW.replace(/%/g, 'retain'),
							controller: 'RetainController'
						})
						.when('/evaluate', {
							templateUrl: CONSTANTS.COMPONENT_VIEW.replace(/%/g, 'evaluate'),
							controller: 'EvaluateController'
						})
						.otherwise({
							redirectTo: '/start'
						});
				});
		}
	);
})();