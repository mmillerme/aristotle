(function () {
	'use strict';

	var REQUIRE_VERSION = '2.1.17';
	var REQUIRE_CSS_VERSION = '0.1.8';
	var ANGULAR_VERSION = '1.3.15';
	var JQUERY_VERSION = '2.1.3';
	var BOOTSTRAP_VERSION = '3.3.4';
	var UI_BOOTSTRAP_VERSION = '0.12.1';

	var RESOURCE_ROOT = '/resources/';
	var LIB_ROOT = RESOURCE_ROOT + 'lib/';
	var VENDOR_ROOT = LIB_ROOT + 'vendor/';

	require.config({
		baseUrl: LIB_ROOT,

		paths: {
			css: VENDOR_ROOT + 'require-' + REQUIRE_VERSION + '/css-' + REQUIRE_CSS_VERSION + '.min',
			jquery: VENDOR_ROOT + 'jquery-' + JQUERY_VERSION + '/jquery.min',
			bootstrap: VENDOR_ROOT + 'bootstrap-' + BOOTSTRAP_VERSION + '/js/bootstrap',

			angular: VENDOR_ROOT + 'angular-' + ANGULAR_VERSION + '/angular.min',
			ngRoute: VENDOR_ROOT + 'angular-' + ANGULAR_VERSION + '/angular-route.min',
			ngResource: VENDOR_ROOT + 'angular-' + ANGULAR_VERSION + '/angular-resource.min',
			ngSanitize: VENDOR_ROOT + 'angular-' + ANGULAR_VERSION + '/angular-sanitize.min',
			ngMessages: VENDOR_ROOT + 'angular-' + ANGULAR_VERSION + '/angular-messages.min',
			'ui.bootstrap': VENDOR_ROOT + 'angular-' + ANGULAR_VERSION + '/ui-bootstrap-tpls-' + UI_BOOTSTRAP_VERSION + '.min',
			PrimitiveWrapper: VENDOR_ROOT + 'angular-' + ANGULAR_VERSION + '/PrimitiveWrapper',
			collapsible: VENDOR_ROOT + 'angular-' + ANGULAR_VERSION + '/collapsible',
			scrollSpy: VENDOR_ROOT + 'angular-' + ANGULAR_VERSION + '/scrollSpy',
			affix: VENDOR_ROOT + 'angular-' + ANGULAR_VERSION + '/affix',
			shuffleArray: VENDOR_ROOT + 'angular-' + ANGULAR_VERSION + '/shuffleArray',

			app: RESOURCE_ROOT + 'app',
			config: RESOURCE_ROOT + 'app.config',
			CONSTANTS: LIB_ROOT + 'constants'
		},

		shim: {
			jquery: { exports: '$' },

			bootstrap: [ 'jquery' ],
			angular: { deps: [ 'jquery' ], 'exports': 'angular' },
			ngRoute: [ 'angular' ],
			ngResource: [ 'angular' ],
			ngSanitize: [ 'angular' ],
			ngMessages: [ 'angular' ],
			'ui.bootstrap': [ 'angular' ],
			PrimitiveWrapper: [ 'angular' ],
			collapsible: [ 'angular' ],
			scrollSpy: [ 'angular' ],
			affix: [ 'angular', 'bootstrap' ],
			shuffleArray: [ 'angular' ]
		}
	});

	require(
		[ 'angular', 'jquery', 'app', 'config' ],
		function (angular, $) {
			$(function () {
				angular.bootstrap(document, [ 'Aristotle' ]);
			});
		}
	);
})();