'use strict';

module.exports = function (req, res, context) {
	var field = new String(req.params.id).split('.')[1];
	if (field) {
		if (context.instance[field] !== undefined) {
			res.send(String(context.instance[field]));
			return;
		} else {
			res.json(400, { message: 'Property does not exist', errors: [ { property: field } ] });
			return;
		}
	}

	return context.continue;
};