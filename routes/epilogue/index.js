'use strict';

var epilogue = require('epilogue');
module.exports = function (app, sequelize) {
	epilogue.initialize({
		app: app,
		sequelize: sequelize,
		base: '/api',
		updateMethod: 'POST'
	});

	var path = require('path');
	var models = app.get('models');
	require('fs').readdirSync(__dirname).forEach(function (file) {
		if (path.extname(file) == '.js' && file != 'index.js') {
			require('./' + path.basename(file, '.js'))(epilogue, models);
		}
	});
};