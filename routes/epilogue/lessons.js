'use strict';

module.exports = function (epilogue, models) {
	var resource = epilogue.resource({
		model: models.Lesson,
		endpoints: [ '/lessons', '/lessons/:id' ],
		include: [ {
			model: models.Chapter,
			include: [ models.Note ]
		} ]
	});

	resource.list.data(function (req, res, context) {
		context.instance = context.instance.map(function (lesson) {
			return {
				id: lesson.id,
				title: lesson.title
			};
		});

		return context.continue;
	});

	var getResourceProperty = require('./utilities/getResourceProperty');
	resource.read.data(function (req, res, context) {
		return getResourceProperty(req, res, context);
	});

	return resource;
};