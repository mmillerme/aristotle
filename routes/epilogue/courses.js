'use strict';

module.exports = function (epilogue, models) {
	var resource = epilogue.resource({
		model: models.Course,
		endpoints: [ '/courses', '/courses/:id' ],
		include: [ {
			model: models.Lesson,
			attributes: [ 'id', 'title' ]
		} ]
	});

	resource.list.data(function (req, res, context) {
		context.instance = context.instance.map(function (course) {
			return {
				id: course.id,
				title: course.title
			};
		});

		return context.continue;
	});

	var getResourceProperty = require('./utilities/getResourceProperty');
	resource.read.data(function (req, res, context) {
		return getResourceProperty(req, res, context);
	});

	return resource;
};