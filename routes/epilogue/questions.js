'use strict';

module.exports = function (epilogue, models) {
	var resource = epilogue.resource({
		model: models.Question,
		endpoints: [ '/questions/', '/questions/:id' ]
	});

	resource.list.fetch.before(function (req, res, context) {
		context.criteria = context.criteria || {};
		context.criteria.LessonId = req.query.lessons;

		return context.continue;
	});

	resource.list.data(function (req, res, context) {
		context.instance = context.instance.map(function (question) {
			try {
				question.answers = JSON.parse(question.answers);
			} catch (x) {
				console.error('Invalid JSON for question ' + question.id + ':\n' + question.answers + '\n\n\n');
				question.answers = [ { content: 'Incorrect' }, { content: 'Incorrect' }, { content: 'Correct', correct: true }, { content: 'Incorrect' } ];
			}

			return question;
		});

		return context.continue;
	});

	return resource;
};