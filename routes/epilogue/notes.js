'use strict';

module.exports = function (epilogue, models) {
	var resource = epilogue.resource({
		model: models.Note,
		endpoints: [ '/notes/', '/notes/:id' ],
		include: [ {
			model: models.Chapter,
			attributes: [ 'id', 'title' ],
			include: [ {
				model: models.Lesson,
				attributes: [ 'id', 'title' ]
			} ]
		} ]
	});

	resource.list.fetch.before(function (req, res, context) {
		context.criteria = context.criteria || {};
		context.criteria.content = { $ne: '' };

		return context.continue;
	});

	return resource;
};