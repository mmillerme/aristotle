'use strict';

var express = require('express');

module.exports = function (app, sequelize) {
	require('./epilogue/')(app, sequelize);
};