'use strict';

module.exports = {
	sequelize: {
		dialect: 'mysql',
		host: 'localhost',
		username: 'root',
		password: 'root',
		database: 'aristotle'
	}
};