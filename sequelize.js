'use strict';

var config = require('./config');
var Sequelize = require('sequelize');

var options = JSON.parse(JSON.stringify(config.sequelize));

var database = options.database;
delete options.database;
var username = options.username;
delete options.username;
var password = options.password;
delete options.password;

module.exports = new Sequelize(database, username, password, options);